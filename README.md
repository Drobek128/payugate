# README #

To install via composer you need to add repositories section

```
"repositories": [
    {
      "type": "vcs",
      "url": "https://Drobek128@bitbucket.org/Drobek128/payugate.git"
    }
  ],
```  

  and then in your require section add:
  
```
  "el-toro/payugate": "dev-master"
```

in controller use PayUGate:
```
  use PayUGate\PayUGate;
```

Example of minimal usage to send order:

```
  //...
  // set $merchantId, privateKey and environment, $yourOrderId

  // create PayUGate object
  $gate = new PayUGate($merchantId, $privateKey, $environment, $yourOrderId);
  //$yourOrderId is not needed, it will be generated unique if not provided

  // set shop currency. Must be same as shop currency at payU site.
  $gate->setOrderCurrency('PLN');

  //set order description
  $gate->setOrderDesciption('Test order');

  //set redirect urls, only success url is needed
  $gate->setUrls('url-to-redirect-after-success', 'url-of-address-which-is-controlled-for-order-status');

  // ...
  // get products info from database, cart or other location

  // add product(s)
  foreach($products as $product) {
    $gate->addProduct($product->name, $product->price, $amount-of-products);
  }

  // set id of customer
  $gate->setCustomerIp(request()->ip());

  //finalize order just before sending it to payUGate
  $gate->finalizeOrder();

  //get response for storing payU orderId to database
  $response = $gate->createPayUResponse();
  $paymentResponse = $response->getResponse();
  $ourOrderId = $paymentResponse->extOrderId;
  $payUOrderId = $paymentResponse->orderId;
  //...
  // save order ids into database

  //redirect. This is needed and payU form will be shown after this redirect.
  header('Location:' . $response->getResponse()->redirectUri);
```

Example of additional information to send to PayU

```
  // ...
  // needed information needs to be provided same as in example above
  // all this data needs to be added before calling $gate->finalizeOrder();

  //set buyer information
  $gate->setBuyer($buyerEmail, $buyerPhone, $firstName, $lastName);

  //set invoice information
  // Invoice can't be used without buyer
  $gate->setInvoice($name, $email, $phone, $invoiceDescription, $street, $postalBox, $postalCode, $city, $countryCode, $tin);

  //...
  // load shipping methods from database
  foreach($shippings as $shipping) {
    $gate->addShippingMethod($shipping->name, $shipping->countryCode, $shipping->price);
  }
  
  //set delivery data
  //delivery data can't be used without at least one shipping
  $gate->setDeliveryData($recipientName, $recipientEmail, $recipientPhone, $street, $postalBox, $postalCode, $city, $state, $countryCode);

```

For checking order status or getting invoice information

```
  //...
  // set $merchantId, privateKey and environment

  // create PayUGate object
  $gate = new PayUGate($merchantId, $privateKey, $environment);

  //... load payUOrderId from database:
  // get order status
  gate->getOrderStatus($payUOrderId);

  // get invoice data in form of []
  $invoice = $gate->getInvoiceData('CT1WQNDWS8170911GUEST000P01');
  //$invoice is array with keys 'name', 'street', 'postalCode', 'city', 'countryCode';

```
